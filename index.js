const app = require('express')();
const path = require('path');

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, '/src/index.html'));
});

app.get('/:id', async (req, res) => {
  const id = req.params.id;
  // respond slowly
  await sleep(50);
  // randomly return the wrong number 10% of the time
  if (Math.random() < 0.1) {
    res.json({
      id: 108,
    });
  }
  res.json({
    id,
  });
});

app.listen(3000);
