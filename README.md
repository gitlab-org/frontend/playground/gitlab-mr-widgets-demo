# GitLab MR Widgets Demo

This is a demo of the GitLab [merge request widgets](https://docs.gitlab.com/ee/user/project/merge_requests/widgets.html).

Currently this repository demos:

- [Security Reports](https://docs.gitlab.com/ee/user/application_security/#use-security-scanning-tools-with-merge-request-pipelines)
- [Status checks](https://docs.gitlab.com/ee/user/project/merge_requests/status_checks.html)
- [Terraform states](https://docs.gitlab.com/ee/user/infrastructure/iac/mr_integration.html)
- [Code Quality](https://docs.gitlab.com/ee/ci/testing/code_quality.html)

## Status Checks

In order to enable Status Checks, you'll need to update Project Settings:

1. Navigate to **Settings** > **Merge Requests**
2. Find the Status Checks section
3. Click on **Add Status Check**
4. Fill the form and submit

Example:

| Field | Value  |
| ------ | ------ |
| Service Name | Example Status Check |
| API to Check | https://localhost:3000/any/endpoint |
| Target Branch | All branches |

Providing an endpoint for the **API to Check** field will be enough to display the Status Checks Widget. If you need more functionality, see [the docs](https://docs.gitlab.com/ee/user/project/merge_requests/status_checks.html). 

## Code Quality

In order to display the code quality widget apply the following diff and create a merge request:

```diff
diff --git a/index.js b/index.js
index e67aa0b..18f790b 100644
--- a/index.js
+++ b/index.js
@@ -12,6 +12,8 @@ app.get('/', function (req, res) {
 });
 
 app.get('/:id', async (req, res) => {
+  return;
+
   const id = req.params.id;
   // respond slowly
   await sleep(50);

```

### Troubleshooting

#### Pipeline does not run locally

- If you see an SSL handshake error and the pipeline do not run locally, go to `/admin/application_settings/network` and disable DNS rebinding.
